-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2018 m. Bal 30 d. 09:51
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.2.3-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homestead`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `films`
--

CREATE TABLE `films` (
  `id` int(10) UNSIGNED NOT NULL,
  `genre_id` int(11) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `premiere_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `films`
--

INSERT INTO `films` (`id`, `genre_id`, `title`, `description`, `premiere_date`, `created_at`, `updated_at`) VALUES
(1, 3, 'Kersytojai', 'Filmas „Keršytojai\" vienoje juostoje apjungia įspūdingiausių „Marvel\" komiksų herojų likimus ir nuotykius.Šioje juostoje superherojai Kapitonas Amerika, Toras, Geležinis Žmogus bei Halkas (akt. Mark Ruffalo) suvienys jėgas ir drauge stos į kovą su blogio jėgomis.Superherojų akiratyje netikėtai pasirodęs priešas kelia grėsmę pasaulio saugumui. Nikas Furis – tarptautinės taikos palaikymo organizacijos SHIELD vadovas suvokia, jog pasaulis, atsidūręs ant katastrofos ribos neišsivers be superherojų pagalbos. Todėl keturi didžiavyriai turi suremti jėgas ir užbėgti įvykiams už akių. Aušta didis mūšis žmonijos istorijoje, galimai pakeisiantis esamą santvarką.Įspūdingi specialieji efektai, keturis kartus daugiau superherojų, nesibaigiantis veiksmas ir kraują kaitinantys nuotykiai!', '2018-05-12', NULL, '2018-04-29 17:52:47'),
(3, 1, 'djhdtjdtjtdjhdtjdtjtdjhdtjdtjt', 'djhdtjdtjtdescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescription', '2018-04-28', '2018-04-29 17:31:30', '2018-04-30 09:47:06');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `genres`
--

CREATE TABLE `genres` (
  `id` int(10) UNSIGNED NOT NULL,
  `genre` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `genres`
--

INSERT INTO `genres` (`id`, `genre`) VALUES
(1, 'Komedija'),
(2, 'Siaubo'),
(3, 'Animacinis'),
(4, 'Fantastinis');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `films`
--
ALTER TABLE `films`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `genres`
--
ALTER TABLE `genres`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
