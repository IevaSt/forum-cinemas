<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\films;
use App\genres;
class PublicPostController extends Controller
{
     public function store(Request $request){
                $this->validate(request(),
            [
                'title'=> 'required',
                'genre'=>'required',
                'description'=> 'required'
            ]);
        films::create([
            'title'=>request('title'),
            'genre_id'=>request('genre'),
            'description'=>request('description'),
            'premiere_date'=>request('pre_date')
        ]);
        return redirect('/');

}
    public function show(){
        $films = films::all();
        $genres = genres::all();
        return view('pages.home', compact('films', 'genres'));
    }
    public function deleteFilm(films $films){
        $films->delete();
        return redirect('/');
    }
    public function editFilm(films $films){
        $genres = genres::all();
        return view('pages.editPost', compact('films', 'genres'));
    }
        public function upEditFilm(Request $request, films $films){
        films::where('id', $films->id)->update($request->only(['title','genre_id', 'description']));
        return redirect('/');
    }
}