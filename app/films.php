<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class films extends Model
{
   protected $fillable = ['genre_id','title','description','premiere_date']; 

 public function genre()
    {
        return $this->hasMany(genres::class);
    }
}
