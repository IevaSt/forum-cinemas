
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}}">
@include('includes/head')

<body>
	@include('includes/header')
<div class="container">
     <div class="row">
        @yield('content')
    </div>
    <hr>
    @include('includes/footer')
</div> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../../dist/js/bootstrap.min.js"></script>
<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
