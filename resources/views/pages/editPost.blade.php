@extends ('layouts/main')
@section('content')
<div class="container">
  <div class="col-md-12">
    <div class="form-area">
      <form role="form" method="post" action="/postupdate/{{$films->id}}/">
        {{csrf_field()}}
        {{METHOD_FIELD('PATCH')}}
        <br style="clear:both">
        <h3 style="margin-bottom: 25px; text-align: center;">Redaguoti Filmo įrašą</h3>
        <div class="form-group">
          <input type="text" class="form-control" name="title" for="title" id="title" value="{{$films ->title}}" required>
        </div>
        <select class="form-control form-control-sm"id="genre_id" name="genre_id">
          @foreach($genres as $genre)
          <option  value="{{($genre->id)}}">
            <h3>{{($genre->genre)}}</h3>
            @endforeach
          </option>
        </select>
        <div class="form-group">
          <textarea class="form-control" type="textarea" name="description" id="description" rows="7">{{$films->description}}</textarea>
        </div>
        <input type="submit" name="submit" value="Siųsti"></input>
      </form>
    </div>
  </div>
</div>
@endsection