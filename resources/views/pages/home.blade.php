@extends ('layouts/main')

@section('content')
	
<div class="container new-post ">
  <div class="">
    <div class="form-area">
      <form role="form" method="post" action="/savefilm">
        {{csrf_field()}}
        <br style="clear:both">
        <h3 style="margin-bottom: 25px; text-align: center;">Naujas filmas</h3>
        <div class="form-group">
          <input type="text" class="form-control" name="title" for="title" id="title" placeholder="Filmo pavadinimas" required >
        </div>
        <select class="form-control form-control-sm" id="genre" name="genre">
          @foreach($genres as $genre)
          <option  value="{{($genre->id)}}">
            <h3>{{($genre->genre)}}</h3>
            @endforeach
          </option>
        </select>
        <br>
        <div class="form-group">
          <textarea class="form-control" type="textarea" name="description" id="description" placeholder="Aprašymas" rows="7" required></textarea>
        </div>
        <div class="form-group">
          <input type="date" class="form-control" name="pre_date" id="pre_date" placeholder="Premjeros data" required>
        </div>
        <input type="submit" name="submit" value="Siųsti"></input>
      </form>
    </div>
  </div>
</div>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Filmas</th>
      <th scope="col">Žanras</th>
      <th scope="col">Aprašymas</th>
      <th scope="col">Premjeros data</th>
    </tr>
  </thead>
  <tbody>
      @foreach($films as $film)
    <tr>
      
          <th scope="row">{{($film->id)}}</th>
          <td>{{($film->title)}}</td>
          <td>{{\App\genres::find($film->genre_id)->genre}}</td>

          <td>{{($film->description)}}</td>
          <td>{{($film->premiere_date)}}</td>
                <td><a class="btn btn-default" href="/editfilm/{{$film->id}}/edit" role="button">Edit</a>
        <a class="btn btn-default" href="/deletefilm/{{$film->id}}/delete" role="button">Delete</a></td>
    </tr>
  @endforeach
  </tbody>
</table>

@endsection